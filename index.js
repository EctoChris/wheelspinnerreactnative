import React from 'react'
import { AppRegistry, View, Text }from 'react-native'
import Header from './src/components/Header'
import Slider from './src/components/Slider'

//Create a component
const App = () => {
  return (
    <View style={{flex: 1}}>
      <Header headerText={"Spin the Wheel"}/>
      <Slider/>
    </View>
  )
}

//Render it to the device
AppRegistry.registerComponent('WheelSpinner',() => App)
