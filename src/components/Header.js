import React from 'react'
import {Text, View} from 'react-native'


const Header = (props) => {
  const {textStyle, viewStyle } = styles
  return (
    <View style={viewStyle}>
      <Text style={textStyle}> {props.headerText} </Text>
    </View>
  )
}

const styles = {
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    paddingTop: 15,
    backgroundColor: 'pink',
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2},
    shadowOpacity: 0.4,
    elevation: 2,
  },
  textStyle: {
    fontSize: 25,
    color: 'black',
    fontWeight: '800'
  }
}


export default Header
