import React, {Component} from 'react'
import {View, PanResponder, Animated,Image, TouchableOpacity, Text } from 'react-native'
import Svg, {Rect} from 'react-native-svg'
import Wheel from '../Assets/Images/colorWheel.png'
//import BarCutout from '../Assets/Images/HollySpinnerBarCutout.png'
import HollyWheel from '../Assets/Images/HollyWheel.png'
import BarCutout from '../Assets/Images/LoadingBar3.png'

export default class Slider extends Component<Props>  {
  constructor(props){
    super(props)
    this.currentDegrees = 0
    this.startingDegrees = 0
    this.lastDegrees = 0
    this.loadingBarProgression = 0
    this.dragging = false
    this.firstTimeLoading = true
    this.SPEED = 3;
    this.AGE_MIN = 5
    this.AGE_MAX = 120
    this.state = {
      marginLeft: 1,
      movingRectMarginRight: 0,
      screenWidth: 0,
      screenHeight: 0,
      halfScreenWidth: 0,
      percentChange: '0deg',
      originX: 0,
      originY: 0,
      degrees: 0,
      degreesString: '0deg',
      wheelSpinnerWidth: 0,
      wheelSpinnerHeight: 0,
      loadingBarProgress: 0,
      loadingBarEmpty: false,
      newLoadingBarProgress: 0,
      loadingBarWidth: 0,
      halfLoadingBarWidth: 0,
      currentAge: 0
    }
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder:(evt, gestureState) => true,
      onPanResponderMove:(evt, gestureState) => {
        if (!this.dragging)
          return

        let mouseX = evt['nativeEvent'].locationX
        let mouseY = evt['nativeEvent'].locationY
        console.log('time limit:', evt.timeStamp)

        if (mouseY > 274)
          return
        let radians = Math.atan2(mouseY - this.state.originY, mouseX - this.state.originX)
        let degrees = radians * (180 / Math.PI) - this.startingDegrees + this.lastDegrees
        console.log('Degrees: ', degrees)

        if (this.lastDegrees < degrees && this.state.degrees * this.SPEED >= 180)
        {
          console.log('Above limit:')
          this.setState({degrees: degrees - 5})
          this.setState({degreesString: this.changeToString(degrees - 5)})
          this.dragging = false
        }
        else if(this.lastDegrees > degrees && this.state.degrees * this.SPEED <= -176)
        {
          console.log('Below Limit:')
          this.setState({loadingBarEmpty: true})
          this.setState({degrees: degrees + 10})
          this.setState({degreesString: this.changeToString(degrees + 10)})
          this.dragging = false
          console.log(this.state.loadingBarEmpty + ' is the loading bar status')
        }
        else {
          this.setState({degreesString: this.changeToString(degrees)})
          this.setState({degrees: degrees})
          this.currentDegrees = degrees
          this.setState({loadingBarEmpty: false})
        }
        //Calculating Loading Bar Progression
       // let percentOfCircle = degrees / 360  //old working one
        let percentOfCircle = degrees / 360 * this.SPEED; //added
        //this.setState({loadingBarProgress: this.state.halfScreenWidth + percentOfCircle * this.state.screenWidth})
        this.setState({loadingBarProgress: this.state.halfLoadingBarWidth + percentOfCircle * this.state.loadingBarWidth})

        this.convertToLoadingBar()
        this.updateCurrentAge()
      },
      onPanResponderStart:(evt, gestureState) => {
        this.dragging = true
        let mouseX = evt['nativeEvent'].locationX
        let mouseY = evt['nativeEvent'].locationY
        //console.log(mouseX + ' is mouseX ' + mouseY + ' is mouseY')
        let radians = Math.atan2(mouseY - this.state.originY, mouseX - this.state.originX)
        this.startingDegrees = radians * (180 / Math.PI)
      },
      onPanResponderEnd:(evt, gestureState) => {
        this.lastDegrees = this.currentDegrees
      }
    })
  }
  convertToLoadingBar(){
    //let degrees = this.currentDegrees //old working one
    let degrees = this.currentDegrees * this.SPEED;;
    this.loadingBarProgression = degrees / 360 * 100
  }
  
  updateCurrentAge(){
    let ageCalculation = this.AGE_MIN + ((this.AGE_MAX - this.AGE_MIN) / 2)
    let newAge = Math.trunc(ageCalculation + (this.loadingBarProgression / 100) * ((this.AGE_MAX - this.AGE_MIN)))
    console.log('Age: ' ,newAge)
    console.log('LoadingBarProgress: ', this.loadingBarProgression)
    if(newAge > this.AGE_MAX)
      newAge = this.AGE_MAX
    else if (newAge < this.AGE_MIN)
      newAge = this.AGE_MIN

    this.setState({currentAge: newAge})
  }

  changeToString(number){
    let newString = '' + number + 'deg'
    return newString
  }

  changeToNumber(string){
    let newNumber = string.slice(string.length - 3, 3)
    return newNumber
  }
  find_dimensions(layout) {
    //To Convert back to full screen loading bar, replace loadingBarWidth with 'screenWidth', and halfLoadingBarWith with 'halfScreenWidth'

    // this.setState({loadingBarProgress: layout.width / 2})
    //this.setState({halfScreenWidth: layout.width / 2})
  }
  findLoadingBarDimensions(layout){
    this.setState({screenWidth: layout.width})
    this.setState({screenHeight: layout.height})
    this.setState({loadingBarProgress: layout.width / 2})
    this.setState({loadingBarWidth: layout.width})
    this.setState({halfLoadingBarWidth: layout.width / 2})
    let ageCalculation = Math.trunc(this.AGE_MIN + ((this.AGE_MAX - this.AGE_MIN) / 2))
    this.setState({currentAge: ageCalculation})
  }
  setOriginOfCircle(layout){
    if (!this.firstTimeLoading)
      return
    this.setState({originX: layout.width / 2})
    this.setState({originY: layout.height / 2})
    this.setState({})
  }
  find_wheelSpinnerDimensions(layout){
    this.setState({wheelSpinnerWidth: layout.width})
    this.setState({wheelSpinnerHeight: layout.height})
  }
  render() {

    const {viewStyle, buttonText, button, spinMeText, ageTextStyle, greyLoadingBarInner, whiteHideBarStyle, textStyle,newLoadingBarInner, newLoadingBarOuter, newLoadingBarSection, loadingBarOuter, loadingBarInner, bottomViewStyle, wheelContainer} = styles
    return (

      <View style={viewStyle} onLayout={(event) => {this.find_dimensions(event.nativeEvent.layout)}}>
        <View>
          <Text style={textStyle}>{this.loadingBarProgression} </Text>
          <Text style={textStyle}>{this.state.loadingBarProgress}</Text>
        </View>
        <Text style={ageTextStyle}>I'm {this.state.currentAge} years old </Text>
        <Text style={spinMeText}>Spin Me </Text>
        <TouchableOpacity
          style={button}
          onPress={this.onPress}
        >
          <Text style={buttonText}> Okay </Text>
        </TouchableOpacity>
        <View style={newLoadingBarSection} onLayout={(event) => {this.findLoadingBarDimensions(event.nativeEvent.layout)}}>
          <View style={newLoadingBarOuter}>
            <Image
              source={BarCutout}
              resizeMode={'stretch'}
              style={{
                height: '100%',
                width: '100%',
                zIndex: 7
              }}
            />
            <View style={[newLoadingBarInner, {width: this.state.loadingBarProgress}, this.state.loadingBarEmpty ? styles.invisible : styles.visible]}>

            </View>
            <View style={greyLoadingBarInner}>
            </View>
            <View style={[whiteHideBarStyle, {left: this.state.loadingBarWidth - 2}]}>
            </View>
          </View>
        </View>
        <Svg
          width="1000"
          height="200"
        >
          <Rect
            x="0"
            y="5"
            width={this.state.lastDistanceMoved}
            height="800"
            fill="rgb(0,0,255)"
            strokeWidth="3"
            stroke="rgb(0,0,0)"
          />
        </Svg>
        <View style={wheelContainer} onLayout={(event) => {this.find_wheelSpinnerDimensions(event.nativeEvent.layout)}}>
          <Animated.Image
            source={HollyWheel}
            onLayout={(event) => {this.setOriginOfCircle(event.nativeEvent.layout)}}
            style={{
              height: '140%',
              width: '140%',
              left: '-20%',
              overflow: 'visible',
              transform: [
                {rotate: this.state.degreesString}
              ]
            }}
          />
        </View>
        <View style={[bottomViewStyle, {height: this.state.wheelSpinnerHeight, width: this.state.wheelSpinnerWidth}]}{...this.panResponder.panHandlers}>

        </View>
      </View>
    )
  }
}

const styles = {
  buttonText: {
    alignSelf: 'center',
    fontSize: 17
  },
  button: {
    width: 120,
    alignSelf: 'center',
    height: 40,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#6F19D8',
    zIndex: 10,
    justifyContent: 'center',
    position: 'absolute',
    bottom: '12%',
    flexDireciton: 'row',
    alignItems: 'center',

},
  whiteHideBarStyle: {
    width: 200,
    height: '100%',
    zIndex: 10,
    backgroundColor: 'white',
    position: 'absolute',
  },
  spinMeText: {
    color: '#ED74BC',
    position: 'absolute',
    bottom:'25%',
    zIndex: 9,
    alignSelf: 'center',
    fontSize: 20
  },
  ageTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    position: 'absolute',
    bottom: '55%'
  },
  viewStyle: {
    flex: 1,
    backgroundColor: 'white'
  },
  invisible: {
    visibility: 'invisible',
    opacity: 0
  },
  visible: {
    visibility: 'visible',
    opacity: 1
  },
  newLoadingBarSection: {
    width: '80%',
    height: '10%',
    position: 'absolute',
    bottom: '45%',
    alignSelf: 'center'
  },
  newLoadingBarOuter: {
    width: '100%',
    height: '100%'
  },
  newLoadingBarInner: {
    backgroundColor: '#6F19D8',
    height: '85%',
    width: '100%',
    left: '1%',
    position: 'absolute',
    marginTop: '2%',
    zIndex: 3
  },
  greyLoadingBarInner: {
    width: '100%',
    height: '90%',
    left: '1%',
    position: 'absolute',
    marginTop: '2%',
    backgroundColor: '#DAD9E9',
    zIndex: 2
  },
  movingBarStyle: {
    marginTop: 100,
    backgroundColor: 'pink',
    height: 60,
  },
  wheelContainer: {
    flex: 1,
    backgroundColor: 'none',
    bottom: -130,
  },
  bottomViewStyle: {
    position: 'absolute',
    bottom: -150,
    backgroundColor: 'none',
    opacity: 0.6
  },
  textStyle: {
    fontSize: 30,
    color: 'red'
  }
}
